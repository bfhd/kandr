/* "fold" long input lines into two or more shorter lines after the last non-blank character that occurs before the n-th column of 
input. Make sure your program does something intelligent with very long lines, and if there are no blanks or tabs before the 
specified column

*/
#include <stdio.h>
#define MAXLINE 1000
#define LINELENGTH 80

char line[MAXLINE]; //current line
char output[MAXLINE];

int get_line(void);

int main() {
    int len;
	extern char output[];

	while ((len = get_line()) > 0) {
		split();
		printf("%s\n", output);
	}
}

// gets a line from console and returns number of chars in the line

int get_line(void) {
	int c, i;
	extern char line[];
	
	for (i = 0; i < MAXLINE -1 && (c = getchar()) != EOF && c != '\n'; ++i) {
		line[i] = c;
	}
	if (c == '\n') {
		line[i] = c;
		++i;
	}
	line[i] = '\0';
	return i;
}


//splits a line based on length
void split(void) {
    int i = 0, length = 0, whitespaceindex = 0;
	extern char line[], output[];
	for (i = 0; i < MAXLINE; ++i) {
		output[i] = '\0';
	}
    //count number of chars (LINELENGTH), insert a \n at the right place
    i = k = j = 0;
	while (line[i] != '\0') {
		if (line[i] == '\t' || line[i] == ' ') { //whitespace
            whitespaceindex = i;
        }
        if (i % LINELENGTH == 0) {
            output[i] = '\n';
        } else {
            output[i] = line[i];
        }
		++i;
	}
}