#include <stdio.h>

int fahrconvert(int value);

int main() {
	int fahr, celsius;
	int lower, upper, step;

	lower = 0;
	upper = 300;
	step = 20;

	fahr = lower;
	while (fahr <= upper) {
		celsius = fahrconvert(fahr);
		printf("%d\t%d\n", fahr, celsius);
		fahr = fahr + step;
	}
	return 0;
}

int fahrconvert(int value) {
	return (5 * (value - 32) / 9);
}
