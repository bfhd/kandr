#include <stdio.h>

int main() {
	int c;
	typedef enum {in, out} word;
	word state = out;
	
	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\t') {
			state = out;
		} else {
			state = in;
		}
		if (state == in) {
			printf("%c",c);
		} else if (state == out) {
			printf("\n");
		}
	}
	return 0;
}
