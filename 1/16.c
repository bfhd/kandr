#include <stdio.h>
#define MAXLINE 1000 // max input line size

int get_line(char line[], int maxline);
void copy(char to[], char from[]);

//print longest input line
int main() {
	int len;
	int max;
	//int mlen = 0;
	char line[MAXLINE];
	char longest[MAXLINE];

	max = 0;
	while ((len = get_line(line, MAXLINE)) > 0) {
		if (len > max) {
			max = len;
			copy(longest, line);
		}
	}
	if (max > 0) {
		printf("%s\nlength: %d\n", longest, max);
	}
	return 0;
}

//getline: read a line into s, returh length
int get_line(char s[], int lim) {
	int c, i;
	for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++i) {
		s[i] = c;
	}
	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s = '\0';
	return i;
}

//copy 'from' into 'to - assume to is big enough
void copy(char to[], char from[]) {
	int i;
	i = 0;
	while ((to[i] = from[i]) != '\0') {
		++i;
	}
}
