#include <stdio.h>
#define MAXWORDLEN 10
 int main() {
	int c = 0, word = 0, i, j, maxword = 0;
	int wordlen[MAXWORDLEN];
	
	for (i = 0; i < MAXWORDLEN; ++i) {
		wordlen[i] = 0;
	}

	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\n' || c == '\t') {
			if (word > MAXWORDLEN) word = MAXWORDLEN;
			++wordlen[word-1];
			word = 0;
		} else {
			++word;
		}
	}
	
	for (i = 0; i < MAXWORDLEN; ++i) {
		if (wordlen[i] > maxword) {
			maxword = wordlen[i];
		}
	}


	for (i = maxword; i > 0; --i) {
		printf("%4d |",i);
		for (j = 0; j < 10; ++j) {
			if (wordlen[j] >= i) {
				printf("* ");
			}
		}
		printf("\n");
	}
	printf("     ----------------------------------\n");
	printf("      1 2 3 4 5 5 7 8 9 10 \n");
	return 0;
}
