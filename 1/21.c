#include <stdio.h>
#define MAXLINE 1000

char line[MAXLINE]; //current line
char output[MAXLINE];

int get_line(void);
void copy(void);

int main() {
	int len;
	extern char output[];

	while ((len = get_line()) > 0) {
		copy();
		printf("%s\n", output);
	}

	return 0;
}

// gets a line from console and returns number of chars in the line

int get_line(void) {
	int c, i;
	extern char line[];
	
	for (i = 0; i < MAXLINE -1 && (c = getchar()) != EOF && c != '\n'; ++i) {
		line[i] = c;
	}
	if (c == '\n') {
		line[i] = c;
		++i;
	}
	line[i] = '\0';
	return i;
}

//copies one string to another and swaps spaces for tabs

void copy(void) {
	int i = 0, j = 0, k = 0;
	extern char line[], output[];
	for (i = 0; i < MAXLINE; ++i) {
		output[i] = '\0';
	}
	i = k = 0;
	while (line[i] != '\0') {
		if (line[i] == '\t') { //swap spaces for tabs (using 8 space tabstops)
			for (j = (8 - (k % 8)); j > 0;++k, --j) {
				output[k] = ' ';
			}
			--k;
		} else {
			output[k] = line[i];
		}
		++i;
		++k;
	}
}

