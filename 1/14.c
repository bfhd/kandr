#include <stdio.h>

int main() {
	int c, i, j;
	int wordarray[26];

	for (i = 0; i < 26; ++i) {
		wordarray[i] = 0;
	}

	while((c = getchar()) != EOF) {
		if (c >= 'a' && c <= 'z') {
			++wordarray[c-'a'];
		}
	}
	
	
	for (i = 0; i < 26; ++i) {
		printf("%4c |",i+'A');
		for (j = 0; j < wordarray[i]; ++j) {
			printf("* ");
		}
		printf("\n");
	}
	printf("     ----------------------------------\n");
	printf("      1 2 3 4 5 5 7 8 9 10 \n");
	
	return 0;
}
