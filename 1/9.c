#include <stdio.h>

int main() {
	int c = 0, count = 0;
	while((c = getchar()) != EOF) {
		if (c == ' ') {	
			if (count == 0) {
				count = 1;
				putchar(c);
			}
		}
		if (c != ' ') {
			count = 0;		
			putchar(c);
		}
	}
	return 0;
}
