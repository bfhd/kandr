#include <stdio.h>

int main () {
	int space = 0, tab = 0, newline= 0, c = 0;
	
	while ((c = getchar()) != EOF) {
		if (c == '\n') ++newline;
		if (c == '\t') ++tab;
		if (c == ' ') ++space;
	}
	printf("Newline: %d\n", newline);
	printf("Tab: %d\n", tab);
	printf("Space: %d\n", space);
	return 0;
}
