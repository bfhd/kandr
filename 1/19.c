#include <stdio.h>
#define MAXLINELEN 1000

int get_line(char s[], int lim);
void reverse(char s[], int l);
void copy(char to[], char from[]);

int main() {
	int len;
	char line[MAXLINELEN];

	while ((len = get_line(line, MAXLINELEN)) > 0) {
		reverse(line, len);
		printf("%s\n",line);
	}

	return 0;
}


int get_line(char s[], int lim) {
	int c, i;
	for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++i) {
		s[i] = c;
	}
	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}

void reverse(char s[], int l) {
	int i;
	char temp[l];
	for (i = 0; i < l; ++i) {
		temp[i] = s[l-(i+1)];
	}
	temp[l] = '\0';
	copy(s, temp);
}


void copy(char to[], char from[]) {
	int i;
	i = 0;
	while ((to[i] = from[i]) != '\0') {
		++i;
	}
}
